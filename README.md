# RPi Rig Rebooter
Programa simple para reiniciar automaticamente una computadora congelada con Raspberry Pi o alguna otra microcontroladora desde el GPIO header
#### Aun en desarrollo

![console](docs/screenshot.png)

usando relevadores de 5V para cerrar el circuito, haciendo ping a las maquinas designadas si no hay respuesta se manda se�al de cierre por los pines del GPIO.

### Logica
 * Ping a las maquinas en un periodo (configurable).
 * Si est� offline -> enviar alta por 5 segundos (mantener el boton de apagado), pausa 5 seg(boton abierto) se�al alta de nuevo por 0.1 segundos (presionar y soltar boton de pc).

### Como
Usando un Raspberry Pi y relevadores a 5V. bastnte basico.

![console](docs/raspberrypi-5v-relay.jpeg)

Dependiendo de la version del Raspberry compilar para las arquitecturas:
`arm6` - (Raspberry Pi A, A+, B, B+, Zero)
`arm7` - (Raspberry Pi 2, 3)

configurar mediante `config.json`
```
./auto-hard-reset-arm7
```

Ejemplo de config.json:
```
{
    "WaitSeconds": 1800, //tiempo entre cada chequeo (en segundos) 
    "StartupCheck" : true, //checar las maquinas al inicio del programa
    "Log" : true, //bitacora
    "RemoteNotify" : false, //notificacion remota (Pushoverapp) //por implementarse
    "TgBotActivate" : false, //Telegram bot
    "TgAPIKey" : "", //Telegram API key
    "TgAdminUserName" : "USERNAME", //Telegram usuario admin 
    "Pushover": false, //Pushover activation
    "PushoverToken": "ACCESS-TOKEN", //Pushover access token
    "PushoverUser": "USER-TOKEN", //Pushover user token
    "Miners": [
        {   "Name": "rig1", "Pin": "40", "Ip": "192.168.0.100", "Info": "GTX 1070's"  },    
        {   "Name": "rig2", "Pin": "38", "Ip": "192.168.0.101", "Info": "RX580's"   }      
    ]
}
```

### Build
##### Requirements
* Raspberry Pi
* Golang >= 1.8.0

Type `go get -u -v gitlab.com/MascoteRPi/Rig-Rebooter.git`

If you are building on your Raspberry Pi, type `go build *.go` in the folder.
If you are building on your workstation type `GOARM=6(or 7) GOARCH=arm GOOS=linux go build *.go`
##### GOARM=6 (Raspberry Pi A, A+, B, B+, Zero) GOARM=7 (Raspberry Pi 2, 3)

### ToDo
* web interface
* JSON-check
* instructions
* statistics
